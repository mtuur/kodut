import java.util.ArrayList;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * @author Marta-Heliiset Tuur
 * @since 2015-11-18
 */

public class HobbitCalculator extends Application {
	Label label0 = new Label("Which Tolkien character are you?");
	Label label = new Label("Choose your weapon");
	Label label2 = new Label("What's your favorite food?");
	Label label3 = new Label("What do you do with your spare time?");
	Label label4 = new Label("What best describes you?");
	Label label5 = new Label("How would you like to die?");

	Pane main;

	ArrayList<String> characters = new ArrayList<String>();

	UserInfo userinfo = new UserInfo();

	public static void main(String[] args) {
		/**
		 * This method is used to start the system.
		 */
		launch(args);
	}

	public void start(Stage stage) {
		/**
		 * This method is used for two reasons. 1st of all, it creates a scene
		 * and adds all the labels and choiceBoxes inside. 2nd of all, it is
		 * also used to design the scene. For instance it changes label's
		 * colour, size and chooses a position for all the children.
		 */
		Scene scene = new Scene(new Group());
		stage.setScene(scene);
		stage.setWidth(500);
		stage.setHeight(500);

		characters.add("file:legolas.jpg");
		characters.add("file:gandalf.jpg");
		characters.add("file:bilbo.jpg");
		characters.add("file:frodo.jpg");
		characters.add("file:gollum.jpg");
		characters.add("file:smaug.jpg");

		label0.setTextFill(javafx.scene.paint.Color.LIGHTCYAN);
		label0.setStyle("-fx-font: 23 arial;");
		label0.setLayoutX(25);
		label0.setLayoutY(15);
		label.setTextFill(javafx.scene.paint.Color.LIGHTCYAN);
		label.setStyle("-fx-font: 18 arial;");
		label.setLayoutX(25);
		label.setLayoutY(44);
		label2.setTextFill(javafx.scene.paint.Color.LIGHTCYAN);
		label2.setStyle("-fx-font: 18 arial;");
		label2.setLayoutX(25);
		label2.setLayoutY(90);
		label3.setTextFill(javafx.scene.paint.Color.LIGHTCYAN);
		label3.setStyle("-fx-font: 18 arial;");
		label3.setLayoutX(25);
		label3.setLayoutY(140);
		label4.setTextFill(javafx.scene.paint.Color.LIGHTCYAN);
		label4.setStyle("-fx-font: 18 arial;");
		label4.setLayoutX(25);
		label4.setLayoutY(193);
		label5.setTextFill(javafx.scene.paint.Color.LIGHTCYAN);
		label5.setStyle("-fx-font: 18 arial;");
		label5.setLayoutX(25);
		label5.setLayoutY(246);

		/*
		 * This is pane which means we have a new environment to put things on.
		 */

		Image img = new Image("file:lord.jpg");
		ImageView imgView = new ImageView(img);
		imgView.setFitHeight(500);
		imgView.setFitWidth(500);

		ChoiceBox weaponChoice = new ChoiceBox(
				FXCollections.observableArrayList("Bow", "Staff", "Axe", "Fork", "Sword"));
		weaponChoice.relocate(25, 65);
		weaponChoice.getSelectionModel().selectFirst();

		ChoiceBox foodChoice = new ChoiceBox(
				FXCollections.observableArrayList("Pizza", "Burgers", "Instant noodles", "eggses", "Bread"));
		foodChoice.relocate(25, 115);
		foodChoice.getSelectionModel().selectFirst();

		ChoiceBox activityChoice = new ChoiceBox(FXCollections.observableArrayList("Sleeep", "Smoke pipe",
				"Adventuureee", "Eat food", "Caressing my precious"));
		activityChoice.relocate(25, 165);
		activityChoice.getSelectionModel().selectFirst();

		ChoiceBox adjChoice = new ChoiceBox(
				FXCollections.observableArrayList("Brave", "Wise", "Stubborn", "Cowardly", "Evil"));
		adjChoice.relocate(25, 215);
		adjChoice.getSelectionModel().selectFirst();

		ChoiceBox deathChoice = new ChoiceBox(FXCollections.observableArrayList("In battle", "I'm immortal",
				"Comfortable at home", "Rich and powerful"));
		deathChoice.relocate(25, 265);
		deathChoice.getSelectionModel().selectFirst();

		Button btn = new Button("Show results");
		btn.relocate(350, 400);

		btn.setOnAction((event) -> {
			/**
			 * This method is activated when btn is pressed It sends necessary
			 * info to a class named UserInfo variable img2 takes from the
			 * 'characters' list a correct character picture from a given
			 * position new scene pops open and img2 is presented.
			 */

			String pickedWeapon = (String) weaponChoice.getValue();
			String pickedFood = (String) foodChoice.getValue();
			String pickedActivity = (String) activityChoice.getValue();
			String pickedAdj = (String) adjChoice.getValue();
			String pickedDeath = (String) deathChoice.getValue();

			userinfo.UserInfo(pickedWeapon, pickedFood, pickedActivity, pickedAdj, pickedDeath);

			// http://java-buddy.blogspot.com.ee/2012/12/open-new-window-in-javafx-2.html
			Image img2 = new Image(characters.get(getClientinfo()));
			ImageView imgView2 = new ImageView(img2);
			imgView2.setFitHeight(500);
			imgView2.setFitWidth(500);

			StackPane secondaryLayout = new StackPane();
			secondaryLayout.getChildren().add(imgView2);

			Scene secondScene = new Scene(secondaryLayout, 500, 500);

			Stage secondStage = new Stage();
			secondStage.setTitle("Result");
			secondStage.setScene(secondScene);

			/**
			 * Sets position of second window, related to primary window.
			 */
			secondStage.setX(250);
			secondStage.setY(100);
			secondStage.show();

		});

		Pane pane = new Pane();

		pane.getChildren().add(imgView);

		pane.getChildren().addAll(weaponChoice, foodChoice, label, label2, label3, label4, label0, label5, btn,
				activityChoice, adjChoice, deathChoice);

		/**
		 * This puts pane on scene. If we don't put the pane here, the empty
		 * scene will be shown.
		 */
		((Group) scene.getRoot()).getChildren().add(pane);
		stage.show();
	}

	private int getClientinfo() {
		/**
		 * This method goes to UserInfo to get the sum of picked options.
		 * returns whichCharater value.
		 */
		int value = userinfo.clientNumberAsACharacter();
		return whichCharacter(value);
	}

	private int whichCharacter(int value) {
		/**
		 * This method chooses according to the sum of chosen answers which list
		 * position to return.
		 */
		System.out.println(value);
		if (value >= 10 && value <= 16) {
			return 0;
		} else if (value >= 17 && value <= 23) {
			return 1;
		} else if (value >= 24 && value <= 30) {
			return 2;
		} else if (value >= 31 && value <= 37) {
			return 3;
		} else if (value >= 38 && value <= 44) {
			return 4;
		} else {
			return 5;
		}
	}
}
