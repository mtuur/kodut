package Kodutoo;
import javafx.scene.control.Label;

public class UserInfo {
	public String pickedWeapon;
	public String pickedFood;
	public String pickedActivity;
	public String pickedAdj;
	public String pickedDeath;

	/**
	 *Constructor takes in punch of values, and sets them according to this class variables.
	 */
	public void UserInfo(String pickedWeapon, String pickedFood, String pickedActivity, String pickedAdj,
			String pickedDeath) {
		this.pickedWeapon = pickedWeapon;
		this.pickedFood = pickedFood;
		this.pickedActivity = pickedActivity;
		this.pickedAdj = pickedAdj;
		this.pickedDeath = pickedDeath;
	}
	/**
	*Different getters to get certain values
	*/
	public String getPickedWeapon() {
		return pickedWeapon;
	}

	public String getPickedFood() {
		return pickedFood;
	}

	public String getPickedActivity() {
		return pickedActivity;
	}

	public String getPickedAdj() {
		return pickedAdj;
	}

	public String getPickedDeath() {
		return pickedDeath;
	}
	/**
	 *This method gives each option in the choiceboxes a numerical value. 
	 */
	public int weaponCalculator() {
		
		if (getPickedWeapon() == "Bow") {
			return 2;
		} else if (getPickedWeapon() == "Staff") {
			return 4;
		} else if (getPickedWeapon() == "Axe") {
			return 6;
		} else if (getPickedWeapon() == "Fork") {
			return 8;
		} else {
			return 10;
		}
	}

	public int foodCalculator() {
		if (getPickedFood() == "Pizza") {
			return 2;
		} else if (getPickedFood() == "Burgers") {
			return 4;
		} else if (getPickedFood() == "Instant noodles") {
			return 6;
		} else if (getPickedFood() == "Eggses") {
			return 8;
		} else {
			return 10;
		}
	}

	public int activityCalculator() {
		if (getPickedActivity() == "Sleeep") {
			return 2;
		} else if (getPickedActivity() == "Smoke pipe") {
			return 4;
		} else if (getPickedActivity() == "Adventuureee") {
			return 6;
		} else if (getPickedActivity() == "Eat food") {
			return 8;
		} else {
			return 10;
		}
	}

	public int adjCalculator() {
		if (getPickedAdj() == "Brave") {
			return 2;
		} else if (getPickedAdj() == "Wise") {
			return 4;
		} else if (getPickedAdj() == "Stubborn") {
			return 6;
		} else if (getPickedAdj() == "Cowardly") {
			return 8;
		} else {
			return 10;
		}
	}

	public int deathCalculator() {
		if (getPickedDeath() == "In battle") {
			return 2;
		} else if (getPickedDeath() == "I'm immortal") {
			return 4;
		} else if (getPickedDeath() == "Comfortable at home") {
			return 6;

		} else {
			return 8;
		}
	}
	
	public String characterStory(){
		return "Tere, minu nimi on bilbo";
		
	}
	/**
	*Sums up all the numeric values according to persons choice and returns value.
	*/
	public int clientNumberAsACharacter() {
		int value = 0;
		value += weaponCalculator();
		value += foodCalculator();
		value += activityCalculator();
		value += adjCalculator();
		value += deathCalculator();
		return value;
	}

}
